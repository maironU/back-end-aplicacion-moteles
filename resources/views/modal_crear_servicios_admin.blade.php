<div class="container-modal" id="modal-crear-servicios-admin">
    <div class="modal"> 
        <h1>Cree su servicio </h1>
        <div class="container-agregar-habitacion">
            <form >
                <input type="text" name="servicio-admin" id="servicio-admin" placeholder = "Servicio">

                <div class="container-file">    
                    <label for="file-servicio-admin">+</label>
                    <input type="file" name="file-servicio-admin" id="file-servicio-admin" onChange = "adjuntar_imagen(this.id)">
                    <span class="file-servicio-admin"></span>
                </div>
                <input id="button" type="submit" name="button" value="Crear" onClick="crear_servicio_admin(this, event)">
            </form>
        </div>
        <span style="color: green" id="mensaje-servicio-admin"></span>
        <div class="cerrar-modal-imagenes-habitacion" onClick="CloseModalServiciosAdmin()">x</div>
    </div>
</div>