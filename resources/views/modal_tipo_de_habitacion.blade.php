<div class="container-modal" id="modal-tipo-habitacion">
    <div class="modal"> 
        <h1>Cree su tipo de habitación </h1>
        <div class="container-agregar-habitacion">
            <form>
                <input type="text" name="tipo_habitacion" id ="tipo_habitacion" placeholder = "Tipo de habitacion">
                <input type="text" name="price" id ="price_tipo_habitacion" placeholder = "Precio mínimo">
                <input id="button-tipo" type="submit" name="button" value="Crear" onClick="crear_tipo_de_habitacion(this, event)">
                <span id="mensaje"></span>
            </form>
        </div>
            <div class="cerrar-modal-imagenes-habitacion" onClick="closeModalTipoHabitacion()">x</div>
    </div>
</div>