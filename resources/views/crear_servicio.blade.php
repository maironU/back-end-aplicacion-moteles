@extends('layouts.app')

@section('name-admin')
    {{auth()->user()->name}}
@endsection

@section('content')
<h1>Crea tu servicio</h1>
<div class="container-form">
    <div class="form-editar">
        <div class="container-servicios">
            <span>Servicios</span>
            <div class="agregar" onClick="openModalCrearServiciosAdmin()">+</div>
        </div>
        <div class="container-todos-los-servicios-creados" id="container-todos-los-servicios-creados">
            @foreach($servicios as $servicio)
                <div class="container-servicio-admin" id="servicio_admin_{{$servicio->servicioadmin_id}}">
                    <img src="{{ Storage::url($servicio->urlservicio)}}" alt="">
                    <div class="delete-servicio-admin" onClick="borrar_servicio_admin({{$servicio->servicioadmin_id}})">
                        <img src="{{ url('logo/delete.png') }}" alt="">
                    </div>
                    <span>{{$servicio->nameservicio}}</span>
                </div>
            @endforeach 
        </div>
    </div>
    @include('modal_crear_servicios_admin')
</div>

@endsection