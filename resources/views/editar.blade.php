@extends('layouts.app')

@section('name-admin')
    {{auth()->user()->name}}
@endsection

@section('content')

    <div class="container-form">
        <form id="formulario_editar" class="form-editar" method="POST" action="{{ route('update',[$informacion->motel_id]) }}" enctype="multipart/form-data">
            {{ method_field('PUT') }}
            {!! csrf_field() !!}

            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

            <div class="container-input">
                <label for="name">Nombre:</label>
                <input type="text" name="name" id="name" value="{{$informacion->name}}">
            </div>
            
            <div class="container-input">
                <label for="name">Precio inicial:</label>
                <input type="text" name="price" id="price" value="{{$informacion->price}}">
            </div>

            <div class="container-input">
                <label for="name">Precio Final:</label>
                <input type="text" name="pricefinal" id="pricefinal" value="{{$informacion->pricefinal}}">
            </div>

            <div class="container-input">
                <label for="name">Informacion:</label>
                <input  type="text" name="information" id="information" value="{{$informacion->information}}">
           </div> 

            <div class="container-input">
                <label for="numberphone">Numero de telefono:</label>
                <input type="number" name="numberphone" id="numberphone" value="{{$informacion->numberphone}}">
            </div>

            <div class="container-input">
                <label for="numbercell">Numero de celular:</label>
                <input type="number" name="numbercell" id="numbercell" value="{{$informacion->numbercell}}">
            </div>

            <div class="container-input">
                <label for="address">Direccion:</label>
                <input type="text" name="address" id="address" value="{{$informacion->address}}">
            </div>

            <div class="container-imagenes-del-motel">
                <span>Imagenes del motel</span>
                <div id="container-imagenes" class="container-imagenes">
                    @foreach($imagenes_del_motel as $imagen)
                    <div class="container-imagen" id="imagen_motel_{{$imagen->image_id}}">
                        <img class="img" src="{{ Storage::url($imagen->imagen)}}" alt="">
                        <div class="delete" onClick="borrar_imagen({{$imagen->image_id}})">
                            <img src="{{ url('logo/delete.png') }}" alt="">
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="container-file">
                    <label for="file">+</label>
                    <input type="file" name="file[]" id="file" onChange = "subir_imagen({{$informacion->motel_id}})"  multiple>
                    <span style='color: green' id="correcto"></span>
                </div>
            </div>

           <div class="container-tipos-habitaciones" id="tipos-de-habitaciones">
               <div class="container-name-tipo dividir">
                    <span>Tipos de habitaciones</span>
                    <div class="agregar" onClick="openModalTipoHabitacion(this, {{$informacion->motel_id}})">+</div>
               </div>
                @foreach($tipos_de_habitaciones as $tipo)
                    <div class="container-tipo dividir" id="tipohabitacion_{{$tipo->tipohabitacion_id}}">
                        <div class="container-name-tipo">
                            <span>{{$tipo->tipo_habitacion}}</span>
                            <div class="container-delete-agregar">
                                <div data-id="{{$tipo->tipo_habitacion}}" class="agregar" onClick="openModalHabitacion(this, {{$tipo->tipohabitacion_id}})">+</div>
                                <img src="{{ url('logo/delete.png') }}" alt="" onClick="borrar_tipo_de_habitacion({{$tipo->tipohabitacion_id}})">
                            </div>
                        </div>
                        <?php $habitaciones = $tipo->habitacion ?>
                            
                        <div id="{{$tipo->tipo_habitacion}}" class="container-habitaciones">
                            @foreach($habitaciones as $habitacion)
                            <div id="habitacion_{{$habitacion->habitacion_id}}" class="container-habitacion-logo">
                                <li class="container-habitacion"  onClick="openModalImagenesHabitacion({{$habitacion->habitacion_id}})">
                                    <span>{{$habitacion->namehabitacion}}</span>
                                    <a>ver</a>
                                </li>
                                <div class="delete-habitacion" onClick="borrar_habitacion({{$habitacion->habitacion_id}})">
                                    <img src="{{ url('logo/delete.png') }}" alt="">
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>      

            <div class="container-historias">
                <span>Historias del motel</span>    
                <div class="container-scroll-historias" id="container-scroll-historias">
                    @foreach($historias as $historia)
                    <div class="container-imagen" id="container-imagen-historia-delete-{{$historia->motelhistory_id}}">
                        <img src="{{ Storage::url($historia->imghistorymotel)}}" alt="">
                        <div class="delete" onClick="borrar_historia({{$historia->motelhistory_id}})">
                            <img src="{{ url('logo/delete.png') }}" alt="">
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="container-file">
                    <label for="file_imagenes_historias">+</label>
                    <input type="file" name="file_imagenes_historias[]" id="file_imagenes_historias" onChange = "subir_historia_del_motel({{$informacion->motel_id}})"  multiple>
                    <span style='color: green' id="mensaje-de-historia"></span>
                </div>
            </div>

            <div class="container-all-servicios">
                <div class="container-servicios">
                    <span>Servicios</span>
                    <div class="agregar" onClick="openModalCrearServiciosMotel(this, {{$informacion->motel_id}})">+</div>
                </div>
                <div class="container-todos-los-servicios-creados" id="container-todos-los-servicios-del-motel">
                    @foreach($servicios_del_motel as $servicio)
                        <div class="container-servicio-admin" id="servicio_motel_{{$servicio->servicio_id}}">
                            <img src="{{ Storage::url($servicio->urlservicio)}}" alt="">
                            <div class="delete-servicio-admin" onClick="borrar_servicio_motel({{$servicio->servicio_id}})">
                                <img src="{{ url('logo/delete.png') }}" alt="">
                            </div>
                            <span>{{$servicio->nameservicio}}</span>
                        </div>
                    @endforeach
                </div>
            </div>

            <button type="submit">Guardar</button>
        </form>
        @include('modal_habitacion')
        @include('modal_tipo_de_habitacion')
        @include('modal_imagenes_habitacion')
        @include('modal_crear_servicios_motel')

    </div>
@endsection 