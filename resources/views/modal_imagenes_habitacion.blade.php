<div class="container-modal" id="modal-imagenes-habitacion">
    <div class="modal_imagenes_habitacion"> 
        <h1>Imágenes de la habitación</h1>
        <ul class="container-imagenes-habitacion" id="container-imagenes-habitacion">

        </ul>
        <div class="container-agregar-habitacion">
            <form>
                <label for="button-imagenes-habitacion">+</label>
                <input type="file" id="button-imagenes-habitacion" name="file_imagenes_habitacion[]" value="Crear" onchange="crear_imagenes_habitacion(this, event)" multiple>
                <span style='color: green' id="mensaje_imagen_habitacion"></span>
            </form>
        </div>
        <div class="cerrar-modal-imagenes-habitacion" onClick="closeModalImagenesHabitacion()">x</div>
    </div>
</div>