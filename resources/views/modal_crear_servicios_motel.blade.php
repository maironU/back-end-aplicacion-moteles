<div class="container-modal" id="modal-crear-servicios-motel">
    <div class="modal-crear-servicios-motel"> 
        <h1>Escoja su servicio </h1>
        <div class="container-todos-servicios-motel-agregar">
            @foreach($servicios_admin as $servicio)
                <div class="container-servicio-motel-agregar">
                    <div class="container-servicio-logo">
                        <img src="{{Storage::url($servicio->urlservicio)}}" alt="">
                        <span>{{$servicio->nameservicio}}</span>
                    </div>
                    <span id="agregado-{{$servicio->servicioadmin_id}}"></span>
                    <img id="logo-agregar-{{$servicio->servicioadmin_id}}" title="Agregar" src="{{ url('logo/agregar.png') }}" alt="" onclick = "agregar_servicio_motel({{$servicio->servicioadmin_id}}, {{$informacion->motel_id}})">
                </div>
            @endforeach
        </div>
        <span style="color: green" id="mensaje-servicio-admin"></span>
        <div class="cerrar-modal-imagenes-habitacion" onClick="CloseModalServiciosMotel()">x</div>
    </div>
</div>