<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <title>Login</title>
</head>
<body>
    <div class="containerForm">
        <form class="form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class = "containerInput">
                <input 
                class="input"
                type="email" 
                name="email" 
                value="{{ old('email') }}"
                placeholder="Ingrese usuario">
                {!! $errors->first('email', '<span>:message</span>') !!}
            </div>
            
            <div class = "containerInput">
                <input 
                class="input"
                type="password" 
                name="password" 
                placeholder="Ingrese contraseña">
                {!! $errors->first('password', '<span>:message</span>') !!}
            </div>
            <button class="button">Acceder</button>
            @if (session()->has('flash'))
                <div align='center'>{{session('flash')}}</div>
            @endif
        </form>
    </div>
</body>
</html>


    