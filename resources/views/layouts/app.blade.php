<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Admin</title>
    <link rel="stylesheet" href="{{ asset('css/estilos.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body id = "overflow">
    
    <div class="container">
        <header>
            <a class="container-logo" href="{{route('dashboard')}}">
                <img src="{{ url('logo/logo.png') }}" alt="">
                <div class="raya"></div>
                <span>MotelSex</span>
            </a>
            
            <div class="container-search">
                <input type="text" placeholder="Ciudad" id="ciudad">

                <div class="container-ciudades">
                    <ul id="ul-ciudades" class="ul-ciudades">
                        
                    </ul>
                </div>
            </div>

            <ul class="ul-profile">
                <li>
                    <img src="{{ url('logo/logo.png') }}" alt="">
                    <span>@yield('name-admin')</span>
                </li>
                <li>
                    <form method="POST" action="{{ route('logout') }}">
                        {{ csrf_field() }}
                        <button>Cerrar</button>
                    </form>
                </li>
            </ul>
        </header>

        <div class="container-profile">
            <img src="{{ url('logo/logo.png') }}" alt="">
            <div class="container-info">
                <h1>@yield('name-admin') </h1>
                <div class="container-names">
                    <span>Mayron Urieles</span>
                    <span>Angel laguna</span>
                </div>
                <span>Software development</span>
            </div>
            @yield('crear-motel')
            @yield('crear-servicios')
        </div>
        @yield('content')
    </div>
    <script src="{{ asset('js/dashboard.js') }}"></script>

</body>
</html>