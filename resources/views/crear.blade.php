@extends('layouts.app')

@section('name-admin')
    {{auth()->user()->name}}
@endsection

@section('content')
<h1>Crea tu motel</h1>
<div class="container-form">
        <form class="form-editar" method="POST" action="{{ route('store') }}" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <div class="container-input">
                <label for="name">Nombre:</label>
                <input type="text" 
                name="name" 
                id="name"
                value="{{ old('name') }}"
                >
                @if($errors->has('name'))
                    <p>{{ $errors->first('name') }}</p>
                @endif
            </div>
            
            <div class="container-input">
                <label for="name">Precio inicial:</label>
                <input type="text" 
                name="price" 
                id="price"
                value="{{ old('price') }}"
                >
                @if($errors->has('price'))
                    <p>{{ $errors->first('price') }}</p>
                @endif
            </div>

            <div class="container-input">
                <label for="name">Precio Final:</label>
                <input type="text" 
                name="pricefinal" 
                id="pricefinal" 
                value="{{ old('pricefinal') }}"
                >
                @if($errors->has('pricefinal'))
                    <p>{{ $errors->first('pricefinal') }}</p>
                @endif
            </div>

            <div class="container-input">
                <label for="name">Informacion:</label>
                <input  type="text"
                name="information" 
                id="information"
                value="{{ old('information') }}"
                >
                @if($errors->has('information'))
                    <p>{{ $errors->first('information') }}</p>
                @endif
           </div> 

            <div class="container-input">
                <label for="numberphone">Numero de telefono:</label>
                <input type="text" 
                name="numberphone" 
                id="numberphone"
                value="{{ old('numberphone') }}"
                >
                @if($errors->has('numberphone'))
                    <p>{{ $errors->first('numberphone') }}</p>
                @endif
            </div>

            <div class="container-input">
                <label for="numbercell">Numero de celular:</label>
                <input type="text" 
                name="numbercell" 
                id="numbercell"
                value="{{ old('numbercell') }}"
                >
                @if($errors->has('numbercell'))
                    <p>{{ $errors->first('numbercell') }}</p>
                @endif
            </div>

            <div class="container-input">
                <label for="address">Direccion:</label>
                <input type="text" 
                name="address" 
                id="address"
                value="{{ old('address') }}"
                >
                @if($errors->has('address'))
                    <p>{{ $errors->first('address') }}</p>
                @endif
            </div>

            <div class="container-input">
                <label for="city">Ciudad:</label>
                <input type="text" 
                name="city" 
                id="city-motel"
                value="{{ old('city') }}"
                >
                <div style="position:relative">
                    <ul id="ciudades-motel" class="ul-ciudades">
                        
                    </ul>
                </div>
                @if($errors->has('city'))
                    <p>{{ $errors->first('city') }}</p>
                @endif
            </div>

            <div class="container-subir-imagenes">
                <span>Subir imagen de perfil</span>
                <div class="container-file">
                    <label for="file-perfil">+</label>
                    <input type="file" name="file-perfil" id="file-perfil" onChange = "adjuntar_imagen(this.id)">
                    <span class="file-perfil"></span>
                </div>
                @if($errors->has('file-perfil'))
                    <p>{{ $errors->first('file-perfil') }}</p>
                @endif
            </div>
            
            <div class="container-subir-imagenes">
                <span>Subir imagenes del motel</sp  an>
                <div class="container-file">
                    <label for="file">+</label>
                    <input type="file" name="file[]" id="file" onChange = "adjuntar_imagen(this.id)" multiple>
                    <span class="file"></span>
                </div>
                @if($errors->has('file'))
                    <p>{{ $errors->first('file') }}</p>
                @endif
            </div>

            <button type="submit">Siguiente</button>
        </form>
@endsection