@extends('layouts.app')

@section('name-admin')
    {{auth()->user()->name}}
@endsection

@section('crear-motel')
    <div class="container-crear-motel">
        <a class="container-crear" href="{{route('nuevo')}}">
            <span>+</span>
        </a>
        <span class="crear-motel">Crear motel</span>
    </div>
@endsection

@section('crear-servicios')
    <div class="container-crear-motel">
        <a class="container-crear" href="{{route('nuevo_servicio')}}">
            <span>+</span>
        </a>
        <span class="crear-motel">Crear Servicios</span>
    </div>
@endsection

@section('content')

    <div class="container-moteles">
        
    @if(Session::has('message'))
        <h2 style="tex-align: center; color: green">{{Session::get('message')}}</h2>
    @endif  
        <ul class="ul-moteles">
            @foreach($moteles as $motel)
                <li>
                    <img src="{{ Storage::url($motel->urlImage)}}" alt="">
                    <div class="container-name">
                        <span>{{ $motel->name }}</span>
                    </div>

                    <div class="logo-opciones" onClick = "openModal({{$motel->motel_id}})">...</div>
                </li>
            @endforeach
            <div class="container-modal" id="modal">
                <div class="modal">
                    <div>
                        <form id="form2" name = "form2" action="" method="GET">
                            <button type="submit">Editar</button>
                        </form>
                    </div>
                    <div>
                        <form id="form" name = "form" action="" method="POST">
                            {{ method_field('DELETE') }}
                            {!! csrf_field() !!}
                            <button type="submit">Eliminar</button>
                        </form>
                    </div>
                    <div class="cerrar-modal" onClick = "closeModal()">Cerrar</div>
                </div>
            </div>
        </ul>
        {{$moteles->links()}}
    </div>

    </div>
@endsection