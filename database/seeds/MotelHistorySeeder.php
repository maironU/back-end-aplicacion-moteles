<?php

use Illuminate\Database\Seeder;

class MotelHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'hacienda.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'malibu.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'hacienda.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'hacienda.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'malibu.jpg',
            'motel_idmotel' => '2'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'malibu.jpg',
            'motel_idmotel' => '2'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'mayron.jpg',
            'motel_idmotel' => '3'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'mayron.mp4',
            'motel_idmotel' => '3'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'mama.jpg',
            'motel_idmotel' => '7'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'paraiso.jpg',
            'motel_idmotel' => '9'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'paraiso.jpg',
            'motel_idmotel' => '9'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'jopo.jpg',
            'motel_idmotel' => '8'
        ]);

        DB::table('motel_histories')->insert([
            'imghistorymotel' => 'acapulco.jpg',
            'motel_idmotel' => '11'
        ]);
    }
}
