<?php

use Illuminate\Database\Seeder;

class TipoHabitacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_habitacions')->insert([
            'tipo_habitacion' => 'Sencillas',
            'price' => '50000',
            'motel_idmotel' => '1'
        ]);

        DB::table('tipo_habitacions')->insert([
            'tipo_habitacion' => 'VIP',
            'price' => '80000',
            'motel_idmotel' => '1'
        ]);

        DB::table('tipo_habitacions')->insert([
            'tipo_habitacion' => 'Sencillas',
            'price' => '50000',
            'motel_idmotel' => '2'
        ]);

    }
}
