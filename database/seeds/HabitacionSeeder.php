<?php

use Illuminate\Database\Seeder;

class HabitacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Senciillas
        DB::table('habitacions')->insert([
            'namehabitacion' => '201',
            'imgprofile' => 'habitacion201S.jpg',
            'tipo_habitacion_idhabitacion' => '1',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '202',
            'imgprofile' => 'habitacion202S.jpg',
            'tipo_habitacion_idhabitacion' => '1',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '203',
            'imgprofile' => 'habitacion203S.jpg',
            'tipo_habitacion_idhabitacion' => '1',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '204',
            'imgprofile' => 'habitacion204S.jpg',
            'tipo_habitacion_idhabitacion' => '1',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '205',
            'imgprofile' => 'habitacion205S.jpg',
            'tipo_habitacion_idhabitacion' => '1',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '206',
            'imgprofile' => 'habitacion206S.jpg',
            'tipo_habitacion_idhabitacion' => '1',
        ]);


        //VIP
        DB::table('habitacions')->insert([
            'namehabitacion' => '201',
            'imgprofile' => 'habitacion201V.jpg',
            'tipo_habitacion_idhabitacion' => '2',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '202',
            'imgprofile' => 'habitacion202V.jpg',
            'tipo_habitacion_idhabitacion' => '2',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '203',
            'imgprofile' => 'habitacion203V.jpg',
            'tipo_habitacion_idhabitacion' => '2',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '204',
            'imgprofile' => 'habitacion204V.jpg',
            'tipo_habitacion_idhabitacion' => '2',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '205',
            'imgprofile' => 'habitacion205V.jpg',
            'tipo_habitacion_idhabitacion' => '2',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '206',
            'imgprofile' => 'habitacion206V.jpg',
            'tipo_habitacion_idhabitacion' => '2',
        ]);

        //Sencillas
        DB::table('habitacions')->insert([
            'namehabitacion' => '201',
            'imgprofile' => 'habitacion201S.jpg',
            'tipo_habitacion_idhabitacion' => '3',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '202',
            'imgprofile' => 'habitacion202S.jpg',
            'tipo_habitacion_idhabitacion' => '3',
        ]);

        DB::table('habitacions')->insert([
            'namehabitacion' => '203',
            'imgprofile' => 'habitacion203S.jpg',
            'tipo_habitacion_idhabitacion' => '3',
        ]);

    }
}
