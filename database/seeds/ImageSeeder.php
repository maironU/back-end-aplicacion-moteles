<?php

use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->insert([
            'imagen' => 'hacienda1.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('images')->insert([
            'imagen' => 'hacienda2.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('images')->insert([
            'imagen' => 'hacienda1.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('images')->insert([
            'imagen' => 'hacienda3.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('images')->insert([
            'imagen' => 'hacienda4.jpg',
            'motel_idmotel' => '1'
        ]);

        DB::table('images')->insert([
            'imagen' => 'malibu1.jpg',
            'motel_idmotel' => '2'
        ]);

        DB::table('images')->insert([
            'imagen' => 'malibu2.jpg',
            'motel_idmotel' => '2'
        ]);
    }
}
