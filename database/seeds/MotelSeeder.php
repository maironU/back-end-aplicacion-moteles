<?php

use Illuminate\Database\Seeder;

class MotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('motels')->insert([
            'name' => 'La Hacienda',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'hacienda.jpg',
            'city_idcity' => '1',
        ]);

        DB::table('motels')->insert([
            'name' => 'Malibu',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'malibu.jpg',
            'city_idcity' => '1',
        ]);

        DB::table('motels')->insert([
            'name' => 'Mayron',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'mayron.jpg',  
            'city_idcity' => '1',
        ]);

        DB::table('motels')->insert([
            'name' => 'Casa De Piedra',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'casa.jpg',
            'city_idcity' => '1',
        ]);

        DB::table('motels')->insert([
            'name' => 'La Monda',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'monda.jpg',
            'city_idcity' => '1',
        ]);

        DB::table('motels')->insert([
            'name' => 'La Chucha',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'chucha.jpg',
            'city_idcity' => '1',
        ]);

        DB::table('motels')->insert([
            'name' => 'Tu mama',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'mama.jpg',
            'city_idcity' => '1',
        ]);

        DB::table('motels')->insert([
            'name' => 'El jopo',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'jopo.jpg',
            'city_idcity' => '1',
        ]);


        DB::table('motels')->insert([
            'name' => 'El Paraiso',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'paraiso.jpg',
            'city_idcity' => '2',
        ]);

        DB::table('motels')->insert([
            'name' => 'Dubai',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'dubai.jpg',
            'city_idcity' => '5',
        ]);

        DB::table('motels')->insert([
            'name' => 'Acapulco',
            'price' =>'35.000',
            'pricefinal' =>'100.000',
            'information' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'numbercell' => '3213860106',
            'numberphone' => '4217867',
            'address' => 'Calle29D2#21A2-85',
            'urlImage' => 'acapulco.jpg',
            'city_idcity' => '5',
        ]);
    }
}
