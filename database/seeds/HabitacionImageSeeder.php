<?php

use Illuminate\Database\Seeder;

class HabitacionImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Hacienda Sencillas
        DB::table('habitacion_images')->insert([
            'imagen' => 'habitacion201.jpg',
            'habitacion_idhabitacion' => '1'
        ]);

        DB::table('habitacion_images')->insert([
            'imagen' => 'habitacion201-2.jpg',
            'habitacion_idhabitacion' => '1'
        ]);

        DB::table('habitacion_images')->insert([
            'imagen' => 'habitacion202.jpg',
            'habitacion_idhabitacion' => '2'
        ]);

        DB::table('habitacion_images')->insert([
            'imagen' => 'habitacion202-2.jpg',
            'habitacion_idhabitacion' => '2'
        ]);
        
        //Hacienda VIP
        DB::table('habitacion_images')->insert([
            'imagen' => 'habitacion201.jpg',
            'habitacion_idhabitacion' => '7'
        ]);

        //Maibu Sencillas
        DB::table('habitacion_images')->insert([
            'imagen' => 'habitacion202-2.jpg',
            'habitacion_idhabitacion' => '13'
        ]);


    }
}
