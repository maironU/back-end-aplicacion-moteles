<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables([
            'cities',
            'motels',
            'tipo_habitacions',
            'habitacions',
            'servicios',
            'users',
            'images',
            'motel_histories',
            'romantic_plans',
            'habitacion_images',
            'tariffs'
        ]);

        $this->call(CitySeeder::class);
        $this->call(MotelSeeder::class);
        $this->call(TipoHabitacionSeeder::class);
        $this->call(HabitacionSeeder::class);
        $this->call(ServicioSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ImageSeeder::class);
        $this->call(MotelHistorySeeder::class);
        $this->call(RomanticPlanSeeder::class);
        $this->call(HabitacionImageSeeder::class);
        $this->call(TariffSeeder::class);

    }

     protected function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
