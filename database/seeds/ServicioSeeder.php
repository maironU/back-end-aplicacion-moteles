<?php

use Illuminate\Database\Seeder;

class ServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servicios')->insert([
            'nameservicio' => 'Parquedadero',
            'urlservicio' =>'parqueadero.png',
            'motel_idmotel' => '1'
        ]);

        DB::table('servicios')->insert([
            'nameservicio' => 'Wifi',
            'urlservicio' =>'wifi.png',
            'motel_idmotel' => '1'
        ]);

        DB::table('servicios')->insert([
            'nameservicio' => 'Tv',
            'urlservicio' =>'tv.png',
            'motel_idmotel' => '1'
        ]);

        DB::table('servicios')->insert([
            'nameservicio' => 'Parquedadero',
            'urlservicio' =>'parqueadero.png',
            'motel_idmotel' => '1'
        ]);

        DB::table('servicios')->insert([
            'nameservicio' => 'Wifi',
            'urlservicio' =>'wifi.png',
            'motel_idmotel' => '1'
        ]);

        DB::table('servicios')->insert([
            'nameservicio' => 'Tv',
            'urlservicio' =>'tv.png',
            'motel_idmotel' => '1'
        ]);

        DB::table('servicios')->insert([
            'nameservicio' => 'Parquedadero',
            'urlservicio' =>'parqueadero.png',
            'motel_idmotel' => '2'
        ]);

        DB::table('servicios')->insert([
            'nameservicio' => 'Wifi',
            'urlservicio' =>'wifi.png',
            'motel_idmotel' => '2'
        ]);

        DB::table('servicios')->insert([
            'nameservicio' => 'Tv',
            'urlservicio' =>'tv.png',
            'motel_idmotel' => '2'
        ]);
    }
}
