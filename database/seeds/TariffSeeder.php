<?php

use Illuminate\Database\Seeder;

class TariffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//La hacienda
        DB::table('tariffs')->insert([
            'tariff' => '1 hora $23.000',
            'tipo_habitacion_idhabitacion' => '1'
        ]);

        DB::table('tariffs')->insert([
            'tariff' => '2 hora $45.000',
            'tipo_habitacion_idhabitacion' => '1'
        ]);

        DB::table('tariffs')->insert([
            'tariff' => '3 hora $70.000',
            'tipo_habitacion_idhabitacion' => '1'
        ]);

        DB::table('tariffs')->insert([
            'tariff' => '4 hora $85.000',
            'tipo_habitacion_idhabitacion' => '1'
        ]);

        //Malibu

        DB::table('tariffs')->insert([
            'tariff' => '1 hora $19.000',
            'tipo_habitacion_idhabitacion' => '3'
        ]);

        DB::table('tariffs')->insert([
            'tariff' => '2 hora $30.000',
            'tipo_habitacion_idhabitacion' => '3'
        ]);

        DB::table('tariffs')->insert([
            'tariff' => '3 hora $45.000',
            'tipo_habitacion_idhabitacion' => '3'
        ]);

        DB::table('tariffs')->insert([
            'tariff' => '4 hora $60.000',
            'tipo_habitacion_idhabitacion' => '3'
        ]);

    }
}
