<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('cities')->insert([
            'namecity' => 'Santa Marta',
        ]);

        DB::table('cities')->insert([
            'namecity' => 'Ciénaga',
        ]);

        DB::table('cities')->insert([
            'namecity' => 'Santa Maria',
        ]);

        DB::table('cities')->insert([
            'namecity' => 'Santa Teresa',
        ]);

        DB::table('cities')->insert([
            'namecity' => 'Fundación',
        ]);

        $faker = factory(App\City::class, 1100)->create();
    }
}
