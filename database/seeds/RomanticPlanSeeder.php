<?php

use Illuminate\Database\Seeder;

class RomanticPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//Planes Romanticos de la Hacienda
        DB::table('romantic_plans')->insert([
            'romanticplan' => 'Habitacion Piedra Preciosa que desees, cuenta con decoracion
            de la habitacion y dos horas de ocupacion por $278.000 pesos',
            'motel_idmotel' => '1'
        ]);

        DB::table('romantic_plans')->insert([
            'romanticplan' => 'Habitacion Piedra Preciosa que desees, cuenta con decoracion
            de la habitacion y dos horas de ocupacion por $278.000 pesos',
            'motel_idmotel' => '1'
        ]);

        DB::table('romantic_plans')->insert([
            'romanticplan' => 'Habitacion Piedra Preciosa que desees, cuenta con decoracion
            de la habitacion y dos horas de ocupacion por $278.000 pesos',
            'motel_idmotel' => '1'
        ]);

        //Planes Romanticos de Malibu

        DB::table('romantic_plans')->insert([
            'romanticplan' => 'Habitacion Piedra Preciosa que desees, cuenta con decoracion
            de la habitacion y dos horas de ocupacion por $240.000 pesos',
            'motel_idmotel' => '2'
        ]);

        DB::table('romantic_plans')->insert([
            'romanticplan' => 'Habitacion Piedra Preciosa que desees, cuenta con decoracion
            de la habitacion y dos horas de ocupacion por $240.000 pesos',
            'motel_idmotel' => '2'
        ]);
    }
}
