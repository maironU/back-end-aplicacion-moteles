<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motels', function (Blueprint $table) {
            $table->bigIncrements('motel_id');
            $table->string('name', 100);
            $table->string('price', 100);
            $table->string('pricefinal', 100);
            $table->string('information', 1000);
            $table->string('numbercell', 100);
            $table->string('numberphone', 100);
            $table->string('address', 100);
            $table->string('urlImage', 100);

            $table->unsignedBigInteger('city_idcity');
            $table->foreign('city_idcity')->references('city_id')->on('cities')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motels');
    }
}
