<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotelHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motel_histories', function (Blueprint $table) {
            $table->bigIncrements('motelhistory_id');
            $table->string('imghistorymotel');
            
            $table->unsignedBigInteger('motel_idmotel');
            $table->foreign('motel_idmotel')->references('motel_id')->on('motels')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motel_histories');
    }
}
