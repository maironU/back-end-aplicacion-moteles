<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRomanticPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('romantic_plans', function (Blueprint $table) {
            $table->bigIncrements('romanticplan_id');
            $table->string('romanticplan', 500);
            $table->unsignedBigInteger('motel_idmotel');
            $table->foreign('motel_idmotel')->references('motel_id')->on('motels')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('romantic_plans');
    }
}
