<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHabitacionImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitacion_images', function (Blueprint $table) {
            $table->bigIncrements('habitacionimage_id');
            $table->string('imagen');

            $table->unsignedBigInteger('habitacion_idhabitacion');
            $table->foreign('habitacion_idhabitacion')->references('habitacion_id')->on('habitacions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitacion_images');
    }
}
