<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHabitacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitacions', function (Blueprint $table) {
            $table->bigIncrements('habitacion_id');
            $table->string('namehabitacion');
            $table->string('imgprofile');
            $table->unsignedBigInteger('tipo_habitacion_idhabitacion');
            $table->foreign('tipo_habitacion_idhabitacion')->references('tipohabitacion_id')->on('tipo_habitacions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitacions');
    }
}
