function openModal(id) {
    var overflow = document.getElementById("overflow");
	overflow.style.overflow = "hidden";
    
    var modal = document.getElementById("modal");
    modal.style.display = 'flex';

    var form = document.forms['form'];
    form.action = "http://192.168.1.7:8000/dashboard/eliminar/"+id;

    var form = document.forms['form2'];
    form.action = "http://192.168.1.7:8000/dashboard/editar/"+id;
}

function openModalHabitacion(elemento, id) {

    var nombre_tipo = elemento.getAttribute("data-id");
    document.getElementById("button").setAttribute("data-tipo", nombre_tipo);

    var overflow = document.getElementById("overflow");
	overflow.style.overflow = "hidden";
    
    var modal = document.getElementById("modal-habitacion");
    modal.style.display = 'flex';

    $("#button").attr("data-id", id);
}

function openModalTipoHabitacion(elemento, id) {

    var overflow = document.getElementById("overflow");
	overflow.style.overflow = "hidden";
    
    var modal = document.getElementById("modal-tipo-habitacion");
    modal.style.display = 'flex';

    $("#button-tipo").attr("data-id", id);
}

function openModalImagenesHabitacion(id){

    console.log(id);

    var overflow = document.getElementById("overflow");
	overflow.style.overflow = "hidden";
    
    var modal = document.getElementById("modal-imagenes-habitacion");
    modal.style.display = 'flex';

    $("#button-imagenes-habitacion").attr("data-id", id);
    imagenesDeLasHabitaciones(id);
}

function openModalCrearServiciosAdmin(){
    var overflow = document.getElementById("overflow");
	overflow.style.overflow = "hidden";
    
    var modal = document.getElementById("modal-crear-servicios-admin");
    modal.style.display = 'flex';
}

function openModalCrearServiciosMotel(){
    var overflow = document.getElementById("overflow");
	overflow.style.overflow = "hidden";
    
    var modal = document.getElementById("modal-crear-servicios-motel");
    modal.style.display = 'flex';
}

function CloseModalServiciosMotel(){
    var overflow = document.getElementById("overflow");
	overflow.style.overflow = "auto";
    
    var modal = document.getElementById("modal-crear-servicios-motel");
    modal.style.display = 'none';
}

function CloseModalServiciosAdmin(){
    var overflow = document.getElementById("overflow");
	overflow.style.overflow = "auto";
    
    var modal = document.getElementById("modal-crear-servicios-admin");
    modal.style.display = 'none';
}

function closeModalHabitacion(){
    var overflow = document.getElementById("overflow")
    overflow.style.overflow = "auto"
    overflow.style.position = "relative"

    var modal = document.getElementById("modal-habitacion");
    modal.style.display = 'none';
}

function closeModalTipoHabitacion(){
    var overflow = document.getElementById("overflow")
    overflow.style.overflow = "auto"
    overflow.style.position = "relative"

    var modal = document.getElementById("modal-tipo-habitacion");
    modal.style.display = 'none';
}

function closeModal() {
    var overflow = document.getElementById("overflow")
	overflow.style.overflow = "auto"
    overflow.style.position = "relative"
    
    var modal = document.getElementById("modal");
    modal.style.display = 'none';
}

function closeModalImagenesHabitacion(){

    var overflow = document.getElementById("overflow")
	overflow.style.overflow = "auto"
    overflow.style.position = "relative"
    
    var modal = document.getElementById("modal-imagenes-habitacion");
    modal.style.display = 'none';
    var cell = document.getElementById("container-imagenes-habitacion");

    if ( cell.hasChildNodes() ){
        while ( cell.childNodes.length >= 1 )   {
            cell.removeChild( cell.firstChild );
        }
    }

    document.getElementById("mensaje_imagen_habitacion").innerHTML = "";
}

function adjuntar_imagen(id){
    var cont = 0;
    for(var i = 0; i < document.getElementById(id).files.length; i++){
        cont++;
    }
    if(cont == 1){
        document.getElementsByClassName(id)[0].innerHTML = cont + " archivo subido";
    }else{
        document.getElementsByClassName(id)[0].innerHTML = cont + " archivos subidos";
    }
}
 
window.addEventListener("load", function(){
    document.getElementById("ciudad").addEventListener("keyup", function(){
        window.fetch(`http://192.168.1.7:8000/api/ciudades/mostrar/${document.getElementById("ciudad").value}`)
		.then(function(res){
			return res.json();
		})
		.then(function(myJson){
            console.log(myJson);

            document.getElementById('ul-ciudades').innerHTML = '';

           for (let index = 0; index < myJson.length; index++) {
                document.getElementById('ul-ciudades').innerHTML += 
                "<li><a href='http://192.168.1.7:8000/dashboard/" + myJson[index].city_id +"' " +">" +myJson[index].namecity+ "</a></li>";   
            }
		})
    })
})

window.addEventListener("load", function(){
    document.getElementById("city-motel").addEventListener("keyup", function(){
        window.fetch(`http://192.168.1.7:8000/api/ciudades/mostrar/${document.getElementById("city-motel").value}`)
		.then(function(res){
			return res.json();
		})
		.then(function(myJson){
            document.getElementById('ciudades-motel').innerHTML = '';

           for (let index = 0; index < myJson.length; index++) {
                document.getElementById('ciudades-motel').innerHTML += 
                `<li><a onClick="agregar_ciudad('${myJson[index].namecity}')"> ${myJson[index].namecity}</a></li>`;   
            }
		})
    })
})

function agregar_ciudad(name){
    document.getElementById('city-motel').value= name;
    document.getElementById('ciudades-motel').innerHTML = '';
}   

function agregar_servicio_motel(servicio_id, motel_id){

    myFormData = new FormData();
    myFormData.append('servicio_id', servicio_id);
    myFormData.append('motel_id', motel_id);

    $(document).ready(function(){
        $.ajax({
            type: 'POST',
            url: `http://192.168.1.7:8000/api/servicios/crear`,
            data: myFormData,
            contentType: false,
            processData: false
        }).done(function(res){
            console.log(res);
            
            document.getElementById("container-todos-los-servicios-del-motel").innerHTML +=`
            <div class="container-servicio-admin" id="servicio_motel_${res[0].servicio_id}">
                <img src="/storage/${res[0].url}" alt="">
                <div class="delete-servicio-admin" onClick="borrar_servicio_motel(${res[0].servicio_id})">
                    <img src="http://192.168.1.7:8000/logo/delete.png" alt="">
                </div>
                <span>${res[0].nameservicio}</span>
            </div>
            `
            $("#logo-agregar-"+servicio_id).remove();  
            document.getElementById("agregado-"+servicio_id).innerHTML = "Agregado";   
        })
    })
}

function subir_imagen(id){
    $(document).ready(function(){
        var myformData = new FormData();

        for(var i=0; i < document.getElementById('file').files.length; i++){
            myformData.append('file_' + i, document.getElementById('file').files[i]);
        }

        $.ajax({
            type: 'POST',
            url: `http://192.168.1.7:8000/api/imagenes/${id}`,
            data: myformData,
            contentType: false,
            processData: false
        }).done(function(res){
            console.log(res);
            for(var i = 0; i < res.length; i++){
                document.getElementById('container-imagenes').innerHTML += 
                    `<div class="container-imagen" id="imagen_motel_${res[i].id}">
                        <img class="img" src="/storage/${res[i].name}" alt="">
                        <div class="delete" onClick="borrar_imagen(${res[i].id})">
                            <img src="http://192.168.1.7:8000/logo/delete.png">
                        </div>
                    </div>  
                    `
            }
            document.getElementById('correcto').innerHTML = "Insertado con exito";
        })
    })
}

function subir_historia_del_motel(id){
    console.log("entre a historia")

    $(document).ready(function(){

        var myformData = new FormData();

        for(var i=0; i < document.getElementById('file_imagenes_historias').files.length; i++){
            myformData.append('file_' + i, document.getElementById('file_imagenes_historias').files[i]);
        }

        $.ajax({
            type: 'POST',
            url: `http://192.168.1.7:8000/api/historiaMoteles/crear/${id}`,
            data: myformData,
            contentType: false,
            processData: false
        }).done(function(res){
            console.log(res);
            for(var i = 0; i < res.length; i++){
                document.getElementById('container-scroll-historias').innerHTML += 
                    `<div class="container-imagen" id="container-imagen-historia-delete-${res[i].id}">
                        <img class="img" src="/storage/${res[i].name}" alt="">
                        <div class="delete" onClick="borrar_historia(${res[i].id})">
                            <img src="http://192.168.1.7:8000/logo/delete.png">
                        </div>
                    </div>  
                    `
            }
            document.getElementById('mensaje-de-historia').innerHTML = "Insertado con exito";
        })
    })
}

function borrar_servicio_admin(id){
    $(document).ready(function(){
        $.ajax({
            type: 'DELETE',
            url: `http://192.168.1.7:8000/api/servicios_admin/delete/${id}`
        }).done(function(res){
            if(res == 1){
                $("#servicio_admin_"+id).remove();     
            }
        })
    })
}

function borrar_servicio_motel(id){
    $(document).ready(function(){
        $.ajax({
            type: 'DELETE',
            url: `http://192.168.1.7:8000/api/servicios/borrar/${id}`
        }).done(function(res){
            if(res == 1){
                $("#servicio_motel_"+id).remove();     
            }
        })
    })
}

function borrar_imagen(id){
    $(document).ready(function(){
        $.ajax({
            type: 'DELETE',
            url: `http://192.168.1.7:8000/api/imagenes/borrar/${id}`,
        }).done(function(res){
            $("#imagen_motel_"+id).remove();
            document.getElementById('correcto').innerHTML = "Eliminado";
        })
    })
}

function borrar_habitacion(id){
    $(document).ready(function(){
        $.ajax({
            type: 'DELETE',
            url: `http://192.168.1.7:8000/api/habitaciones/borrar/${id}`,
        }).done(function(res){
            $("#habitacion_"+id).remove();
        })
    })
}

function borrar_tipo_de_habitacion(id){
    $(document).ready(function(){
        $.ajax({
            type: 'DELETE',
            url: `http://192.168.1.7:8000/api/tipoHabitaciones/borrar/${id}`,
        }).done(function(res){
            $("#tipohabitacion_"+id).remove();
        })
    })
}

function borrar_imagen_de_habitacion(id){
    $(document).ready(function(){
        $.ajax({
            type: 'DELETE',
            url: `http://192.168.1.7:8000/api/imagenes_habitaciones/borrar/${id}`
        }).done(function(res){
            $("#container-imagen-habitacion-delete-"+id).remove();
            document.getElementById("mensaje_imagen_habitacion").innerHTML = "Borrado con exito";

        })
    })
}

function borrar_historia(id){
    $(document).ready(function(){
        $.ajax({
            type: 'DELETE',
            url: `http://192.168.1.7:8000/api/historiaMoteles/borrar/${id}`
        }).done(function(res){
            $("#container-imagen-historia-delete-"+id).remove();
            document.getElementById("mensaje-de-historia").innerHTML = "Borrado con exito";
        })
    })
}

function crear_servicio_admin(elemento, evt){
    evt.preventDefault();
    var servicio = document.getElementById("servicio-admin").value;
    var file = document.getElementById('file-servicio-admin').files[0];

    var myFormData = new FormData();
    myFormData.append('file_servicio_admin', file);
    myFormData.append('servicio', servicio);
    $(document).ready(function(){
        $.ajax({
            type: 'POST',
            url: `http://192.168.1.7:8000/api/servicios_admin/crear`,
            data: myFormData,
            contentType: false,
            processData: false
        }).done(function(res){
            if(res == 0){
                document.getElementById("mensaje-servicio-admin").innerHTML = 'Por favor llene todos los campos';
            }else{
                document.getElementById("servicio-admin").value = " ";
                document.getElementById("mensaje-servicio-admin").innerHTML = 'Servicio creado';
                document.getElementById('file-servicio-admin').value = "";
                document.getElementsByClassName('file-servicio-admin')[0].innerHTML ="";

                document.getElementById("container-todos-los-servicios-creados").innerHTML += `
                <div class="container-servicio-admin" id="servicio_admin_${res[0].id}">
                    <img src="/storage/${res[0].name}" alt="">
                    <div class="delete-servicio-admin" onClick="borrar_servicio_admin(${res[0].id})">
                        <img src="http://192.168.1.7:8000/logo/delete.png" alt="">
                    </div>
                    <span>${servicio}</span>
                </div>
                `
            }
        })
    })
}   

function crear_tipo_de_habitacion(elemento, evt){
     
     evt.preventDefault();
 
    var tipo_habitacion = document.getElementById("tipo_habitacion").value;
    var price = document.getElementById("price_tipo_habitacion").value;
    var id_motel = elemento.getAttribute("data-id");
 
    if(tipo_habitacion != ""){
         $(document).ready(function(e){
             $.ajax({
                 type: 'POST',
                 url: "http://192.168.1.7:8000/api/tipoHabitaciones/crear",
                 data: {"tipo_habitacion": tipo_habitacion, "price": price, "id": id_motel},
                 dataType: 'json'
             }).done(function(res){                
                document.getElementById("tipo_habitacion").value = "";
                 closeModalTipoHabitacion();
                 document.getElementById("tipos-de-habitaciones").innerHTML +=`
                 <div class="container-tipo dividir" id="tipohabitacion_${res}">
                    <div class="container-name-tipo">
                        <span>${tipo_habitacion}</span>
                        <div class="container-delete-agregar">
                            <div data-id="${tipo_habitacion}" class="agregar" onClick="openModalHabitacion(this, ${res})">+</div>
                            <img src="http://192.168.1.7:8000/logo/delete.png" alt="" onClick="borrar_tipo_de_habitacion(${res})">
                        </div>
                    </div>

                    <div id="${tipo_habitacion}" class="container-habitaciones">

                    </div>
                </div>
                 `
             })
         })
     }else{
         document.getElementById("mensaje").innerHTML = "Escriba un tipo de habitacion";
     }
     
 }

function crear_habitacion(elemento, evt){

   var data_tipo = elemento.getAttribute("data-tipo");
   console.log(data_tipo);

    evt.preventDefault();

    var habitacion = document.getElementById("habitacion").value;
    var id_tipo_habitacion = $("#button").data("id");
    
    console.log(id_tipo_habitacion);

    if(habitacion != ""){
        $(document).ready(function(e){
            $.ajax({
                type: 'POST',
                url: "http://192.168.1.7:8000/api/habitaciones/crear",
                data: {"habitacion": habitacion, "id": id_tipo_habitacion},
                dataType: 'json'
            }).done(function(res){
                document.getElementById("habitacion").value = "";
                closeModalHabitacion();
                document.getElementById(data_tipo).innerHTML += `
                    <div id="habitacion_${res}" class="container-habitacion-logo">
                        <li class='container-habitacion' onClick="openModalImagenesHabitacion(${res})">
                            <span>${habitacion}</span>
                            <a>ver</a>
                        </li>

                        <div class="delete-habitacion" onClick="borrar_habitacion(${res})">
                            <img src="http://192.168.1.7:8000/logo/delete.png" alt="">
                        </div>
                    </div>

                `
            })
        })
    }else{
        document.getElementById("mensaje").innerHTML = "Escriba un nombre";
    }
}

function crear_imagenes_habitacion(){
    var id_habitacion = document.getElementById("button-imagenes-habitacion").getAttribute("data-id");

    console.log(id_habitacion);

    var myFormData = new FormData();

    for(var i=0; i < document.getElementById('button-imagenes-habitacion').files.length; i++){
        myFormData.append('file_' + i, document.getElementById('button-imagenes-habitacion').files[i]);
    }
    $(document).ready(function(){
        $.ajax({
            type: 'POST',
            url: `http://192.168.1.7:8000/api/imagenes_habitaciones/crear/${id_habitacion}`,
            data: myFormData,
            contentType: false,
            processData: false
        }).done(function(res){
            for(var i = 0; i < res.length; i++){
                document.getElementById("container-imagenes-habitacion").innerHTML +=`
                <div class='container-imagen-habitacion-delete' id="container-imagen-habitacion-delete-${res[i].id}">
                    <li>
                        <img src='/storage/${res[i].name}' />
                    </li>

                    <div class="delete" onClick="borrar_imagen_de_habitacion(${res[i].id})">
                        <img src="http://192.168.1.7:8000/logo/delete.png" alt="">
                    </div>
                </div>
                `

                document.getElementById("mensaje_imagen_habitacion").innerHTML = "Insertado con exito";
            }
        })
    })
}

function imagenesDeLasHabitaciones(id){
    $(document).ready(function(){
        $.ajax({
            type: 'GET',
            url: `http://192.168.1.9:8000/api/imagenes_habitaciones/mostrar/${id}`,
        }).done(function(res){
            console.log(res)
            for(var i = 0; i < res.length; i++){
                document.getElementById("container-imagenes-habitacion").innerHTML +=`
                    <div class='container-imagen-habitacion-delete' id="container-imagen-habitacion-delete-${res[i].habitacionimage_id}">
                        <li>
                            <img src='/storage/${res[i].imagen}' />
                        </li>
                        <div class="delete" onClick="borrar_imagen_de_habitacion(${res[i].habitacionimage_id})">
                            <img src="http://192.168.1.7:8000/logo/delete.png" alt="">
                        </div>
                    </div>
                `
            }
        })
    })
}