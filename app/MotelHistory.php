<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Motel;

class MotelHistory extends Model
{
    protected $primaryKey = 'motelhistory_id';

    public function motel()
    {
        return $this->belongsTo('App\Motel');
    }

    public static function moteles_con_historias()
    {  
        return Motel::select('motel_id', 'name', 'urlImage')->with(array('motelHistory' => function($q){
            $q->select('motel_idmotel', 'imghistorymotel');
        }));
    }

    public static function moteles_con_historias_en_la_ciudad($city_id)
    {
        return Motel::select('motel_id', 'name', 'urlImage')->with(array('motelHistory' => function($q){
            $q->select('motel_idmotel', 'imghistorymotel');
        }))->join('cities', 'motels.city_idcity', 'cities.city_id')->where('city_idcity', $city_id);
    }

    public static function motel_con_su_historia($name_motel)
    {
        return Motel::select('motel_id', 'name', 'urlImage')->with(array('motelHistory' => function($q){
            $q->select('motel_idmotel', 'imghistorymotel');
        }))->where('name', $name_motel);
    }
}
