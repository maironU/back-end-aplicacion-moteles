<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio;
use App\ServicioAdmin;
use App\Motel;

class ServicioController extends Controller
{
    public function index()
    {
        return Servicio::all();
    }

    public function show($id)
    {
        $servicios = Motel::servicios_del_motel($id)->get();

        return response()->json($servicios, 200);
    }

    public function create(Request $request){
        $servicio_admin = ServicioAdmin::where('servicioadmin_id', $request->servicio_id)->get();
        $array = array();

        $servicio_motel = new Servicio;
        $servicio_motel->nameservicio = $servicio_admin[0]->nameservicio;
        $servicio_motel->urlservicio = $servicio_admin[0]->urlservicio;
        $servicio_motel->motel_idmotel = $request->motel_id;
        $servicio_motel->save();
        
        $array[0] = ['servicio_id' => $servicio_motel->id, 'nameservicio' => $servicio_motel->nameservicio, 'url' => $servicio_motel->urlservicio];
        return $array;
    }

    public function delete($id){
        $servicio_motel = Servicio::where('servicio_id', $id);
        $servicio_motel->delete();

        return 1;
    }

}
