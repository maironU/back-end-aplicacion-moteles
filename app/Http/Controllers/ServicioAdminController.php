<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServicioAdmin;

class ServicioAdminController extends Controller
{

    public function create(){
        $servicios = ServicioAdmin::servicios_admin();
        return view('crear_servicio', compact('servicios'));
    }

    public function create_service_admin(Request $request){

        $array = array();

        if($request->hasFile('file_servicio_admin') && $request->servicio !=null){
            $file = $request->file('file_servicio_admin');
            $name = time().$file->getClientOriginalName();
            $file->move(storage_path().'/app/public/', $name);
    
            $servicio = new ServicioAdmin;
            $servicio->nameservicio = $request->servicio;
            $servicio->urlservicio = $name;
            $servicio->save();
            $array[0] = ["name" => $name, "id" => $servicio->id];

        }else{
            return 0;
        }
        return $array;
    }

    public function delete_service_admin($id = null){
        $servicio_admin = ServicioAdmin::where('servicioadmin_id', $id);
        $servicio_admin->delete();

        return 1;
    }
}
