<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Motel;
use App\MotelHistory;

class MotelHistoryController extends Controller
{

    public function index(){
        $moteles_con_historias = MotelHistory::moteles_con_historias()->get();

        return response()->json($moteles_con_historias, 200);
    }

    public function HistoryMotel($city_id = null)
    {
        $moteles_con_historias_en_la_ciudad = MotelHistory::moteles_con_historias_en_la_ciudad($city_id)->get();
        
        return response()->json($moteles_con_historias_en_la_ciudad, 200);
    }

    public function OnlyHistoryMotel($name_motel = null){
        $motel_con_su_historia = MotelHistory::motel_con_su_historia($name_motel)->get();
        
        return response()->json($motel_con_su_historia, 200);
    }

    public function create(Request $request, $id = null){
        $array = array();

        for($i=0; $i<count($request->file());$i++){
            $file = $request->file();
            $name = time().$file['file_'.$i]->getClientOriginalName();
            $file['file_'.$i]->move(storage_path().'/app/public/', $name);

            $historia = new MotelHistory;
            $historia->imghistorymotel = $name;
            $historia->motel_idmotel = $id;
            $historia->save();
            $array[$i] = ["name" => $name, "id" => $historia->id];
        }

        return $array;
    }

    public function delete($id = null){
        $historia = MotelHistory::where('motelhistory_id', $id);
        $historia->delete();

        return 1;
    }
}

    