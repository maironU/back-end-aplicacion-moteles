<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Habitacion;

class HabitacionController extends Controller
{
    public function index()
    {
        return Habitacion::all();
    }

    public function show($id)
    {
        return Habitacion::where('habitacion_id', $id)->get();
    }

    public function create(Request $request)
    {   
        $habitacion = new Habitacion;
        $habitacion->namehabitacion = $request->habitacion;
        $habitacion->tipo_habitacion_idhabitacion = $request->id;
        $habitacion->save();

        return $habitacion->id;
    }

    public function delete($id = null){
        $habitacion = Habitacion::where('habitacion_id', $id);
        $habitacion->delete();

        return 1;
    }
}
