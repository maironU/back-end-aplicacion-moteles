<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


class AlmacenamientoController extends Controller
{
    public function index($imagen){
    	$store = storage_path().'/app/public/';
        $exists = Storage::disk('public')->exists($imagen);
        $urlFile = ($exists) ? $store.$imagen :'';

        return Image::make($urlFile)->response();
    }	
}


