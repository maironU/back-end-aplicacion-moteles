<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HabitacionImage;

class ImagenHabitacionController extends Controller
{
    public function index($id = null){
        $imagenes_habitacion = HabitacionImage::where('habitacion_idhabitacion', $id)->get();

        return $imagenes_habitacion;
    }

    public function create(Request $request, $id = null){
        $array = array();

        for($i=0; $i<count($request->file());$i++){
            $file = $request->file();
            $name = time().$file['file_'.$i]->getClientOriginalName();
            $file['file_'.$i]->move(storage_path().'/app/public/', $name);

            $imagenes_habitacion = new HabitacionImage;
            $imagenes_habitacion->imagen = $name;
            $imagenes_habitacion->habitacion_idhabitacion = $id;
            $imagenes_habitacion->save();
            $array[$i] = ["name" => $name, "id" => $imagenes_habitacion->id];
        }

        return $array;
    }

    public function delete($id = null){
        $imagen_habitacion = HabitacionImage::where('habitacionimage_id', $id);
        $imagen_habitacion->delete();

        return 1;
    }
}
