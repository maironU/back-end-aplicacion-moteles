<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Motel;
use App\Image;
use App\City;
use App\ServicioAdmin;
use App\Servicio;

use Session;    

class MotelController extends Controller
{

    public function show($motel = null){
        $motelHistory = Motel::select('motels.name','motels.urlImage','motel_histories.motel_idmotel')->join('motel_histories','motels.motel_id','=','motel_histories.motel_idmotel')->groupBy('motel_histories.motel_idmotel','motels.name','motels.urlImage')->where('city_idcity', $city_id)->get();  

        $response = []; 

        foreach ($motelHistory as $motel) {
            $response[] = $this->transform($motel); 
        } 
    }

	public function index()
    {
    	$AllMotels =  Motel::all();
        return response()->json($AllMotels, 200);
    }

    public function information($id){
        $informacion = Motel::select('information')->where('motel_id', $id)->get();

        return response()->json($informacion[0]->information, 200);
    }

    public function contact($motel){
        $contacto = Motel::select('motel_id', 'numbercell', 'numberphone', 'address')->where('name', $motel)->get();

        return response()->json($contacto, 200);
    }

    public function MotelForCity($city_id = null)
    {
        $MotelForCity = Motel::where('city_idcity', $city_id)->get();
    	return response()->json($MotelForCity, 200);
    }

    //CRUD MOTEL

    public function create(){
        return view('crear');
    }

    public function store(Request $request, Motel $motel) {

        $required = request()->validate([
    		'name' => 'required',
    		'price' => 'required',
            'pricefinal' =>'required',
            'information' =>'required',
            'numbercell' =>'required',
            'numberphone' =>'required',
            'address' =>'required',
            'city' => 'required',
            'file' => 'required',
            'file-perfil' => 'required'
    	],[

    		'name.required' => 'El campo nombre es obligatorio',
   			'price.required' => 'El campo precio es obligatorio',
            'pricefinal.required' =>'El campo precio final es obligatorio',
            'information.required'=>'El campo informacion es obligatorio',
            'numbercell.required' =>'El campo numero de celular es obligatorio',
            'numberphone.required' =>'El campo numero de telefono es obligatorio',
            'address.required' =>'El campo direccion es obligatorio',
            'city.required' =>'El campo ciudad es obligatorio',
            'file.required' =>'Por favor sube imagenes del motel',
            'file-perfil.required' =>'Por favor sube una imagen de perfil',
        ]);
        
        $id = City::select('city_id')->where('namecity', $required['city'])->first();

        if($request->hasFile('file-perfil')){
            $file = $request->file('file-perfil');
            $perfil = time().$file->getClientOriginalName();
            $file->move(storage_path().'/app/public/', $perfil);
        }   

        $motel = Motel::create([
            'name'=>$required['name'] , 
            'price'=>$required['price'],
            'pricefinal'=> $required['pricefinal'],
            'information' => $required['information'],
            'numbercell' => $required['numbercell'],
            'numberphone' => $required['numberphone'],
            'address' => $required['address'],
            'urlImage' => $perfil,
            'city_idcity' => $id->city_id,
        ]);
        
        if($request->hasFile('file')){
            for($i=0; $i<count($request->file('file'));$i++){
                $file = $request->file('file');
                $name = time().$file[$i]->getClientOriginalName();
                $file[$i]->move(storage_path().'/app/public/', $name);

                $imagenes = new Image;
                $imagenes->imagen = $name;
                $imagenes->motel_idmotel = $motel->id;
                $imagenes->save();
            }
        }   
        return redirect()->route('dashboard')->with('message', 'Motel creado Correctamente');

    }
    
    public function destroy($id)
    {
        $motel = Motel::where('motel_id', $id);
        $motel->delete();
    	return redirect()->route('dashboard');
    } 

    public function editar($id)
    {
        $informacion = Motel::informacionPrincipal($id)->first();
        $imagenes_del_motel = Motel::imagenes_del_motel($id)->get();
        $tipos_de_habitaciones = Motel::tipos_de_habitaciones($id)->get();
        $historias = Motel::historias_del_motel($id)->get();
        $servicios_admin = ServicioAdmin::servicios_admin();
        $servicios_del_motel = Motel::servicios_del_motel($id)->get();
        return view('editar', compact('informacion','imagenes_del_motel', 'tipos_de_habitaciones','historias', 'servicios_admin', 'servicios_del_motel'));
    } 

    public function update(Request $request, Motel $motel) {

        $motel->name = $request->name;
        $motel->price = $request->price;
        $motel->pricefinal = $request->pricefinal;
        $motel->information = $request->information;
        $motel->numbercell = $request->numbercell;
        $motel->numberphone = $request->numberphone;
        $motel->address = $request->address;

        $motel->save();
    	return redirect()->route('dashboard')->with('message', 'Motel actualizado Correctamente');;
    }
}	
