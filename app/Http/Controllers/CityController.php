<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    public function ciudades($name=null){

    	if(Empty($name)){
    		return response()->json([], 200);
    	}else{
    		$ciudades = City::select('city_id','namecity')->where('namecity', 'Like', '%'.$name.'%')->limit(5)->get();
    		return response()->json($ciudades, 200);
    	}
    }
}
