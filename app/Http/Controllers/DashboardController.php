<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Motel;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index(){
        $moteles =  Motel::paginate(5);
        return view('dashboard', compact('moteles'));
    }

    public function MotelsForCity($city_id = null) 
    {        
        if(empty($city_id)){
            $moteles =  Motel::paginate(5);
            return view('dashboard', compact('moteles'));
        }
        $moteles = Motel::where('city_idcity', $city_id)->paginate(5);
        return view('dashboard', compact('moteles'));
    }
}
