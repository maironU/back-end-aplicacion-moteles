<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoHabitacion;
use App\Motel;

class TipoHabitacionController extends Controller
{
    public function index()
    {
        return TipoHabitacion::all();
    }

    public function show($motel)
    {
        $tipos_de_habitaciones = Motel::tipos_de_habitaciones($motel)->get();

        return response()->json($tipos_de_habitaciones, 200);
    }

    public function create(Request $request){
        $tipo_habitacion = new TipoHabitacion;
        $tipo_habitacion->tipo_habitacion = $request->tipo_habitacion;
        $tipo_habitacion->price = $request->price;
        $tipo_habitacion->motel_idmotel = $request->id;
        $tipo_habitacion->save();

        return $tipo_habitacion->id;
    }

    public function delete($id = null){
        $tipo_habitacion = TipoHabitacion::where('tipohabitacion_id', $id);
        $tipo_habitacion->delete();

        return 1;
    }
}
