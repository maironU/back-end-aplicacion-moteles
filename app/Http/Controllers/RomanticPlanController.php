<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RomanticPlan;

class RomanticPlanController extends Controller
{
    public function show($id = null)
    {
        $planes_romanticos = RomanticPlan::planes_romanticos($id)->get();

        return $planes_romanticos;

        return response()->json($planes_romanticos, 200);
    }
}
