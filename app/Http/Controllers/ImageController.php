<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Motel;
use App\TipoHabitacion;
use App\Habitacion;
use App\Servicio;
use App\RomanticPlan;
use App\HabitacionImage;
use App\Tariff;

class ImageController extends Controller
{

    public function index($id = null)
    {
        $imagenes_del_motel = Motel::imagenes_del_motel_con_su_informacion($id)->get();
        
        return response()->json($imagenes_del_motel, 200);
        
    }

   public function indexx($motel = null)
    {
        $Imagenes = Motel::select('images.motel_idmotel', 'motels.name','motels.information'
        ,'motels.price', 'motels.pricefinal','motels.numbercell', 'motels.numberphone', 'motels.address')
        ->join('images','motel_id','=','motel_idmotel')
        ->join('tipo_habitacions','images.motel_idmotel','=','tipo_habitacions.motel_idmotel')
        ->where('motels.name',$motel)
        ->groupBy('images.motel_idmotel','motels.name','motels.information','motels.price', 
        'motels.pricefinal','motels.numbercell', 'motels.numberphone', 'motels.address')
        ->get();

        $response = []; 

        foreach ($Imagenes as $motel) {
            $response[] = $this->transform($motel); 
        }

        return response()->json($response, 200);
    }

    public function transform(Motel $motel) {

        $imagenes = Image::where('motel_idmotel',$motel->motel_idmotel)->get();
        $tipo_habitaciones = TipoHabitacion::where('motel_idmotel',$motel->motel_idmotel)->get();
        $servicio = Servicio::where('motel_idmotel',$motel->motel_idmotel)->get();
        $planes = RomanticPlan::where('motel_idmotel',$motel->motel_idmotel)->get();

        $response = [];

        foreach ($tipo_habitaciones as $tipo) {
            $response[] = $this->transformTipo($tipo); 
        }


        //Arry de imagenes
        $imagen = [];

        foreach ($imagenes as $key) {
            $imagen[] =  [
                'imagen' => $key->imagen,
            ];
        }


        //Array de Servicios
        $servicios = [];

        foreach ($servicio as $key) {
            $servicios[] =  [
                'servicio' => $key->nameservicio,
                'imagen' => $key->urlservicio,
            ];
        }

        //Array de planes romanticos

        $planes_romanticos = [];

        foreach ($planes as $key) {
            $planes_romanticos[] =  [
                'plan' => $key->romanticplan,
            ];
        }

        //Objeto Completo Del Motel
        return [
            'motel' => $motel->name,
            'imagenes' => $imagen,
            'informacion' => $motel->information,
            'precio' => $motel->price,
            'preciofinal' => $motel->pricefinal,
            'tipo_habitacion' => $response,
            'servicios' => $servicios,
            'planes_romanticos' => $planes_romanticos,
            'numerocelular' => $motel->numbercell,
            'numerophone' => $motel->numberphone,
            'direccion' => $motel->address,
        ];
    }

     public function transformTipo(TipoHabitacion $tipo) {

        $habitaciones = Habitacion::where('tipo_habitacion_idhabitacion',$tipo->tipohabitacion_id)->get();
        $tarifas = Tariff::where('tipo_habitacion_idhabitacion',$tipo->tipohabitacion_id)->get();

        $response = []; 

        foreach ($habitaciones as $image) {
            $response[] = $this->transformHabitacionImages($image); 
        }        

        $tarifa = [];

        foreach ($tarifas as $item) {
            $tarifa[] =  [
                'tarifa' => $item->tariff,
            ];
        }

        return [
        	'tipo_habitacion' => $tipo->tipo_habitacion,
        	'tarifa_min' => $tipo->price,
            'habitaciones' => $response,
            'tarifas' => $tarifa,
        ];
    }

    public function transformHabitacionImages(Habitacion $image) {

        $habitacion_images = HabitacionImage::where('habitacion_idhabitacion',$image->habitacion_id)->get();


        $habitacionimages = [];

        foreach ($habitacion_images as $item) {
            $habitacionimages[] =  [
                'imagen' => $item->imagen,
            ];
        }

        return [
        	'habitacion' => $image->namehabitacion,
        	'imagen' => $habitacionimages,
        ];
    }

    public function create($id = null, Request $request){
        
        $array = array();

        for($i=0; $i<count($request->file());$i++){
            $file = $request->file();
            $name = time().$file['file_'.$i]->getClientOriginalName();
            $file['file_'.$i]->move(storage_path().'/app/public/', $name);

            $imagenes = new Image;
            $imagenes->imagen = $name;
            $imagenes->motel_idmotel = $id;
            $imagenes->save();
            $array[$i] = ["name" => $name, "id" => $imagenes->id];
        }

        return $array;
        
    }

    public function delete($id = null){
        $imagen = Image::where('image_id', $id);
        $imagen->delete();

        return 1;
    }
}
