<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;
use App\Motel;
use App\TipoHabitacion;
use App\Habitacion;
use App\Servicio;
use App\RomanticPlan;
use App\HabitacionImage;
use App\Tariff;

class Motel extends Model
{

    protected $primaryKey = 'motel_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'pricefinal','information','numbercell','numberphone','address','urlImage','city_idcity'
    ];

    public function romanticPlan(){
        return $this->hasMany('App\RomanticPlan');
    }

    public function servicios(){
    	return $this->hasMany('App\Servicio');
    }

    public function tipoHabitacion(){
    	return $this->hasMany('App\TipoHabitacion');
    }

    public function image(){
    	return $this->hasMany('App\Image','motel_idmotel', 'motel_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function motelHistory(){
        return $this->hasMany('App\MotelHistory', 'motel_idmotel', 'motel_id');
    }

    public static function informacionPrincipal($id)
    {
        return Motel::where('motel_id', '=', $id);
    }

    public static function imagenes_del_motel($id)
    {
        return Image::select('images.image_id','images.imagen')->where('motel_idmotel', '=', $id);
    }

    public static function imagenes_del_motel_con_su_informacion($id)
    {
        return Motel::with('image')->where('motel_id', $id);
    }

    public static function servicios_del_motel($id)
    {
        return Servicio::where('motel_idmotel', '=', $id);
    }

    public static function tipos_de_habitaciones($id)
    {
        return TipoHabitacion::with(array('tariff','habitacion' => function($q){
            $q->with('habitacionImage');
        }))->where('motel_idmotel',$id);
    }

    public static function historias_del_motel($id)
    {
        return MotelHistory::select('motelhistory_id', 'imghistorymotel')->where('motel_idmotel', $id);
    }
}
