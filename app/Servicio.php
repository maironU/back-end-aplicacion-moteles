<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    public function motel()
    {
        return $this->belongsTo('App\Motel');
    }
}
