<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Habitacion;

class TipoHabitacion extends Model
{
    public function motel()
    {
        return $this->belongsTo('App\Motel');
    }

    public function habitacion()
    {
    	return $this->hasMany('App\Habitacion','tipo_habitacion_idhabitacion','tipohabitacion_id');
    }

    public function tariff()
    {
    	return $this->hasMany('App\Tariff','tipo_habitacion_idhabitacion','tipohabitacion_id');
    }

    
}
