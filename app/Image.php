<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imagen', 'motel_idmotel'
    ];

    public function motel()
    {
        return $this->belongsTo('App\Motel');
    }
    
}
