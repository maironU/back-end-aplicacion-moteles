<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HabitacionImage extends Model
{
    public function habitacion()
    {
        return $this->belongsTo('App\Habitacion');
    }
}
