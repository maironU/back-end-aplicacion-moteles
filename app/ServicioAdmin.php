<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ServicioAdmin;

class ServicioAdmin extends Model
{
    public static function servicios_admin(){
        return ServicioAdmin::all();
    }
}
