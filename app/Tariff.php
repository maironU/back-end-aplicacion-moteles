<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    public function tipoHabitacion()
    {
        return $this->belongsTo('App\TipoHabitacion');
    }
}
