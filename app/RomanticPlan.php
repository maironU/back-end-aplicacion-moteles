<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RomanticPlan extends Model
{
    public function motel()
    {
        return $this->belongsTo('App\Motel');
    }

    public static function planes_romanticos($id)
    {
        return RomanticPlan::where('motel_idmotel', $id); 
    }
}
