<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function motel()
    {
        return $this->hasMany('App\Motel');
    }
}
