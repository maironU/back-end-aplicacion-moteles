<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Habitacion extends Model
{
    public function tipoHabitacion()
    {
        return $this->belongsTo('App\tipoHabitacion');
    }

    public function habitacionImage(){
    	return $this->hasMany('App\HabitacionImage', 'habitacion_idhabitacion', 'habitacion_id');
    }
}
