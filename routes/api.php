<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Moteles
//Route::get('moteles/historia/{id}', 'MotelController@show');
Route::get('moteles/mostrar/','MotelController@index');
Route::get('moteles/mostrar/{city_id?}','MotelController@MotelForCity');

//Info de cada motel en el Modal

//Imagen de cada Motel
Route::get('almacenamiento/{imagen}', 'AlmacenamientoController@index')
    ->where(['file' => '(.*?)\.(jpg|png|jpeg|svg)$']);

//Video de cada motel

//Servicios motel
Route::get('servicios/mostrar','ServicioController@index');
Route::get('servicios/mostrar/{id}','ServicioController@show');
Route::post('servicios/crear', 'ServicioController@create');
Route::delete('servicios/borrar/{id}', 'ServicioController@delete');


//Servicios admin
Route::post('servicios_admin/crear', 'ServicioAdminController@create_service_admin');
Route::delete('servicios_admin/delete/{id}', 'ServicioAdminController@delete_service_admin');

//Tipo De Habitaciones
Route::get('tipoHabitaciones/mostrar','TipoHabitacionController@index');
Route::get('tipoHabitaciones/mostrar/{motel}','TipoHabitacionController@show');
Route::post('tipoHabitaciones/crear','TipoHabitacionController@create');
Route::delete('tipoHabitaciones/borrar/{id}','TipoHabitacionController@delete');

//Habitaciones
Route::get('habitaciones/mostrar','HabitacionController@index');
Route::get('habitaciones/mostrar/{id}','HabitacionController@show');
Route::post('habitaciones/crear','HabitacionController@create');
Route::delete('habitaciones/borrar/{id}','HabitacionController@delete');

//Imagenes de las habitaciones
Route::get('imagenes_habitaciones/mostrar/{id}','ImagenHabitacionController@index');
Route::post('imagenes_habitaciones/crear/{id}','ImagenHabitacionController@create');
Route::delete('imagenes_habitaciones/borrar/{id}','ImagenHabitacionController@delete');

//Ciudades
//Route::get('ciudades/mostrar','CityController@index');
Route::get('ciudades/mostrar/{name?}', 'CityController@ciudades');

//Usuarios
Route::get('usuarios/mostrar','UserController@index');
Route::get('usuarios/mostrar/{id}', 'UserController@show');

//Historias de Moteles
Route::get('historiaMoteles/mostrar','MotelHistoryController@index');
Route::get('historiaMoteles/mostrar/{city_id}', 'MotelHistoryController@HistoryMotel')->where('city_id','[0-9]+');
Route::get('historiaMoteles/mostrar/{name_motel}', 'MotelHistoryController@OnlyHistoryMotel');
Route::post('historiaMoteles/crear/{id}', 'MotelHistoryController@create');
Route::delete('historiaMoteles/borrar/{id}', 'MotelHistoryController@delete');

//Galeria de imagenes del motel
Route::get('imagenes/{motel}','ImageController@index');
Route::post('imagenes/{motel}', 'ImageController@create');  
Route::delete('imagenes/borrar/{motel}', 'ImageController@delete');

//Informacion del motel
Route::get('informacion/{id}', 'MotelController@information');

//id del motel 
Route::get('contacto/{motel}', 'MotelController@contact');

//Planes Romanticos del motel
Route::get('planes/mostrar/{id}', 'RomanticPlanController@show');