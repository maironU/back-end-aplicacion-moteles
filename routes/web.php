<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', 'Auth\LoginController@showLoginForm')->middleware('guest');

//Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::get('dashboard/{city_id?}', 'DashboardController@MotelsForCity')->name('dashboard')->where('city_id','[0-9]+');;

Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//CRUD MOTELES
Route::get('dashboard/crear/motel','MotelController@create')->name('nuevo');
Route::post('dashboard/create','MotelController@store')->name('store');

//Nuevo Servicio Admin
Route::get('dashboard/crear/servicio', 'ServicioAdminController@create')->name('nuevo_servicio');

Route::delete('dashboard/eliminar/{motel}','MotelController@destroy');
Route::get('dashboard/editar/{motel}','MotelController@editar');
Route::put('dashboard/update/{motel}', 'MotelController@update')->name('update');

